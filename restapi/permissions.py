from rest_framework.permissions import IsAuthenticated


class IsAuthorOrReadOnly(IsAuthenticated):

    def has_object_permission(self, request, view, obj):
        if request.method in ('GET', 'HEAD', 'OPTIONS', 'POST'):
            return True

        return obj.author == request.user.profile
