from django.contrib.auth import authenticate, login, logout
from rest_framework import exceptions, permissions, views, viewsets, generics
from rest_framework.response import Response


from restapi.permissions import IsAuthorOrReadOnly
from restapi.models import User, Post, Profile
from taggit.models import Tag

from restapi.serializers import (
    UserSerializer, PostSerializer, AuthLoginSerializer, TagSerializer)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [IsAuthorOrReadOnly]

    def create(self, request):
        profile = request.user.profile

        serializer = PostSerializer(
            data=request.data, context=self.get_serializer_context())

        if not serializer.is_valid():
            raise exceptions.ValidationError(serializer.errors)

        serializer.save(author=profile)
        return Response(serializer.data)


class PosttagsListView(generics.ListAPIView):
    def get_queryset(self):
        return Post.objects.filter(tags__id__in=[self.kwargs['pk']])
    serializer_class = PostSerializer


class TagsListView(generics.ListCreateAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer


class TagView(generics.RetrieveDestroyAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer


class AuthView(views.APIView):
    def get(self, request, format=None):
        user = request.user
        if user.is_anonymous:
            return Response(
                {'user': None}
            )
        return Response(
            {'user': UserSerializer(user, context={'request': request}).data}
        )


class AuthLoginView(views.APIView):
    permission_classes = [permissions.AllowAny]
    serializer_class = AuthLoginSerializer

    def post(self, request, format=None):
        data = request.data

        auth_serializer = AuthLoginSerializer(data=data)
        auth_serializer.is_valid(raise_exception=True)

        username = request.data.get('username')
        password = request.data.get('password')

        user = authenticate(request, username=username, password=password)
        if user is None:
            raise exceptions.AuthenticationFailed

        if not Profile.objects.filter(user=user):
            Profile.objects.create(
                user=user, fullname=user.username
            )

        login(request, user)
        return Response(
            {'user': UserSerializer(user, context={'request': request}).data}
        )


class AuthLogoutView(views.APIView):
    def get(self, request, format=None):
        logout(request)
        return Response({'user': None})


class NotFoundView (views.APIView):
    def get(self, request):
        raise exceptions.NotFound

    def post(self, request):
        raise exceptions.NotFound

    def put(self, request):
        raise exceptions.NotFound

    def delete(self, request):
        raise exceptions.NotFound
