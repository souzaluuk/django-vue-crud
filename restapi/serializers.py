from django.contrib.auth.models import User
from rest_framework import serializers

from restapi.models import Post, Profile
from taggit.models import Tag


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        exclude = ['user', 'id']


class UserSerializer(serializers.HyperlinkedModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = [
            'id',
            'url',
            'username',
            'email',
            'password',
            'profile'
        ]
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        profile_data = validated_data.pop('profile')

        user = User.objects.create_user(**validated_data)
        Profile.objects.create(user=user, **profile_data)

        return user

    def update(self, instance, validated_data):
        if not validated_data.get('profile') is None:
            profile_data = validated_data.pop('profile')
            profile = instance.profile
            for key, value in profile_data.items():
                setattr(profile, key, value)
            profile.save()

        for key, value in validated_data.items():
            if key == 'password' and value:
                instance.set_password(value)
                continue
            setattr(instance, key, value)
        instance.save()

        return instance


class TagSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Tag
        fields = '__all__'
        extra_kwargs = {'slug': {'read_only': True}}


class PostSerializer (serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(read_only=True)
    author = ProfileSerializer(read_only=True)
    tags = serializers.ListField(source='tag_list', required=False, default = [])

    class Meta:
        model = Post
        fields = '__all__'

    def create(self, validated_data):
        tag_list = validated_data.pop('tag_list')

        post = Post.objects.create(**validated_data)
        post.tags.add(*tag_list)
        post.save()
        return post

    def update(self, instance, validated_data):
        if not validated_data.get('tag_list') is None:
            tag_list = validated_data.pop('tag_list')
            instance.tags.set(*tag_list, clear=True)

        for key, value in validated_data.items():
            setattr(instance, key, value)

        instance.save()
        return instance


class AuthLoginSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(
        required=True,
        style={'input_type': 'password'}
    )
