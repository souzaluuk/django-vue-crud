from django.db import models
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField
from taggit.managers import TaggableManager


class Profile (models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='profile')
    fullname = models.CharField(
        max_length=100, blank=False, null=False, verbose_name='fullname')
    is_admin = models.BooleanField(default=False)

    AUTHOR = 'AUTHOR'
    EDITOR = 'EDITOR'
    ROLES = (
        (AUTHOR, 'AUTHOR'),
        (EDITOR, 'EDITOR'),
    )

    role = models.CharField(
        max_length=6,
        choices=ROLES,
        default=EDITOR,
    )


class Post (models.Model):
    title = models.CharField('Título', max_length=150, blank=False)
    body = RichTextField('Conteúdo', blank=True, null=True)
    start_date = models.DateField(
        'Data de Início',
        auto_now_add=True,
        null=True,
        blank=True
    )
    update_at = models.DateTimeField('Atualizado em', auto_now=True)
    author = models.ForeignKey(
        Profile,
        on_delete=models.CASCADE,
        related_name='post'
    )
    disabled = models.BooleanField(default=False)

    EDITING = 'ED'
    FINISHED = 'FI'
    STATUS_POST_CHOICES = (
        (EDITING, 'EDITING'),
        (FINISHED, 'FINISHED'),
    )

    status = models.CharField(max_length=2,
                              choices=STATUS_POST_CHOICES,
                              default=EDITING,
                              )

    tags = TaggableManager()

    def __str__(self):
        return self.title

    def tag_list(self):
        return self.tags.names()
