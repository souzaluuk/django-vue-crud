# -*- coding: utf-8 -*-
from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User



class UserModelForm(forms.modelform):
    class meta:
        model = User
        fields = ['email','first_name','last_name','username','password']
        widgets = {
            'email': forms.TextInput(attrs={'class':'form-control','max-length':255}),
            'first_name': forms.TextInput(attrs={'class':'form-control','max-length':255}),
            'last_name': forms.TextInput(attrs={'class':'form-control','max-length':255}),
            'username': forms.TextInput(attrs={'class':'form-control','max-length':255}),
            'password': forms.TextInput(attrs={'class':'form-control','max-length':255})
        }

        error.messages = {
            'first_name': {
                'required': 'Esse campo é obrigatório.'
            },
            'last_name': {
                'required': 'Esse campo é obrigatório.'
            },
            'email': {
                'required': 'Esse campo é obrigatório.'
            },
            'password': {
                'required': 'Esse campo é obrigatório.'
            },
            'username': {
                'required': 'Esse campo é obrigatório.'
            },

        
        }

