from django.test import SimpleTestCase
from django.urls import resolve, reverse
from restapi.views import (
    AuthLoginView, AuthLogoutView, AuthView,
    NotFoundView, UserViewSet, PostViewSet,
    TagsListView, TagView, PosttagsListView,
)


class TestUrls(SimpleTestCase):

    def test_user_list_url_is_resolved(self):
        url = reverse('user-list')
        self.assertEquals(resolve(url).func.cls, UserViewSet)

    def test_user_detail_url_is_resolved(self):
        url = reverse('user-detail', args='pk')
        self.assertEquals(resolve(url).func.cls, UserViewSet)

    def test_post_list_url_is_resolved(self):
        url = reverse('post-list')
        self.assertEquals(resolve(url).func.cls, PostViewSet)

    def test_post_detail_url_is_resolved(self):
        url = reverse('post-detail', args='pk')
        self.assertEquals(resolve(url).func.cls, PostViewSet)

    def test_auth_url_is_resolved(self):
        url = reverse('auth')
        self.assertEquals(resolve(url).func.cls, AuthView)

    def test_AuthLoginView_url_is_resolved(self):
        url = reverse('auth-login')
        self.assertEquals(resolve(url).func.cls, AuthLoginView)

    def test_AuthLogoutView_url_is_resolved(self):
        url = reverse('auth-logout')
        self.assertEquals(resolve(url).func.cls, AuthLogoutView)

    def test_NotFoundView_url_is_resolved(self):
        url = reverse('not-found')
        self.assertEquals(resolve(url).func.cls, NotFoundView)

    def test_PosttagsListView_url_is_resolved(self):
        url = reverse('tag-list-posts', args='0')
        self.assertEquals(resolve(url).func.cls, PosttagsListView)

    def test_TagsListView_url_is_resolved(self):
        url = reverse('tag-list')
        self.assertEquals(resolve(url).func.cls, TagsListView)

    def test_TagView_url_is_resolved(self):
        url = reverse('tag-detail', args='0')
        self.assertEquals(resolve(url).func.cls, TagView)