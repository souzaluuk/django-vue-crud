from django.urls import reverse
from django.test import TestCase
from rest_framework import status, test
from restapi.models import Post, Profile, User
from taggit.models import Tag


class TestViews(TestCase):
    def setUp(self):
        self.client = test.APIClient()
        self.user = User.objects.create_superuser(
            id=1,
            username='Usertest',
            password='admin',
        )
        self.profile = Profile.objects.create(
            user=self.user,
            fullname='Userfullnametest',
            is_admin=True,
            role='AUTHOR',
        )
        self.post = Post.objects.create(
            author=self.profile,
            id=1,
            title='Testtitle',
            body='Testbody',
            status='FI',
        )
        Tag.objects.create(id=1, slug='testtag', name='testtag')
        self.post.tags.add('testtag')
        self.post.save()
        self.user2 = User.objects.create_superuser(
            id=2,
            username='Usertest2',
            password='admin2',
            email='user.test@mail.com',
        )
        self.profile2 = Profile.objects.create(
            user=self.user2,
            fullname='User2fullnametest',
            is_admin=True,
            role='AUTHOR',
        )
        self.post2 = Post.objects.create(
            author=self.profile2,
            id=2,
            title='Testtitle',
            body='Testbody',
            status='FI',
        )
        self.client.login(username='Usertest', password='admin')
        self.user_list_url = reverse('user-list')
        self.user_detail_url = reverse('user-detail', args='1')
        self.user_detail_delete_url = reverse('user-detail', args='2')
        self.post_list_url = reverse('post-list')
        self.post_detail_url = reverse('post-detail', args='1')
        self.tag_list_post = reverse('tag-list-posts', args='1')
        self.tag_list = reverse('tag-list')
        self.tag_detail = reverse('tag-detail', args='1')

    def test_user_list_get(self):
        response = self.client.get(self.user_list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_detail_get(self):
        response = self.client.get(self.user_detail_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.data.get('username'))
        self.assertEquals(response.data.get('id'), self.user.id)
        self.assertEquals(response.data.get('username'), self.user.username)
        self.assertEquals(response.data.get('email'), self.user.email)
        profile = response.data.get('profile')
        self.assertEquals(profile.get('fullname'), self.profile.fullname)
        self.assertEquals(profile.get('role'), self.profile.role)
        self.assertEquals(profile.get('is_admin'), self.profile.is_admin)

    def test_user_detail_patch(self):
        response = self.client.patch(self.user_detail_url, {
            'email': 'userchange@gmail.com',
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.user.refresh_from_db()
        self.assertEquals(response.data.get('email'), self.user.email)

    def test_user_detail_delete(self):
        response = self.client.delete(self.user_detail_delete_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        count = User.objects.filter(id=self.user2.id).count()
        self.assertEquals(count, 0)

    def test_post_list_get(self):
        response = self.client.get(self.post_list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_list_post(self):
        data = {'title': 'testtitle', 'body': 'testbody'}
        response = self.client.post(self.post_list_url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.data.get('title', 'body'))
        self.assertEquals(response.data.get(
            'title', 'body'), 'testtitle', 'testbody')

    def test_post_detail_get(self):
        response = self.client.get(self.post_detail_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.data.get('id', 'title'))
        self.assertEquals(response.data.get('id', 'title'), 1, 'Testtitle')

    def test_post_detail_patch(self):
        response = self.client.patch(self.post_detail_url, {
            'body': 'bodychange',
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data.get('body'), 'bodychange')
        self.post.refresh_from_db()
        self.assertEquals(response.data.get('body'), self.post.body)

    def test_post_detail_delete(self):
        response = self.client.delete(self.post_detail_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        count = Post.objects.filter(id=self.post.id).count()
        self.assertEquals(count, 0)

    def test_tag_list_post_get(self):
        response = self.client.get(self.tag_list_post)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_tag_list_get(self):
        response = self.client.get(self.tag_list)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_tag_list_post(self):
        data = {'id': '2', 'name': 'testtag2'}
        response = self.client.post(self.tag_list, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        count = Tag.objects.all().count()
        self.assertEquals(count, 2)

    def test_tag_detail_get(self):
        response = self.client.get(self.tag_detail)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEquals(response.data.get('id', 'name'), 1, 'testtag')

    def test_tag_detail_delete(self):
        response = self.client.delete(self.tag_detail)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        count = Tag.objects.all().count()
        self.assertEquals(count, 0)
