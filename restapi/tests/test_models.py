from datetime import date

from django.contrib.auth.models import User
from django.db import utils
from django.test import TestCase
from restapi.models import Post, Profile
from taggit.models import Tag


class TestProfile (TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='Usertest',
            password='admin',
            email='user.test@mail.com',
        )
        self.profile = Profile.objects.create(
            user=self.user,
            fullname='Userfullnametest',
            is_admin=True,
            role='AUTHOR',
        )

    def test_profile_create(self):
        self.assertEquals(Profile.objects.count(), 1)
        Profile.objects.create(
            user=User.objects.create_user(
                username='user2',
                password='user2',
                email='user2@user2.com',
            ),
            fullname='profile2',
            is_admin=True,
            role='AUTHOR',
        )
        self.assertEquals(Profile.objects.count(), 2)

    def test_profile_create_fullname_field(self):
        profile = Profile.objects.get(id=self.profile.id)
        self.assertEquals(profile.fullname, 'Userfullnametest')

    def test_profile_create_is_admin_field(self):
        profile = Profile.objects.get(id=self.profile.id)
        self.assertTrue(profile.is_admin)

    def test_profile_create_role_field(self):
        profile = Profile.objects.get(id=self.profile.id)
        self.assertEquals(profile.role, 'AUTHOR')

    def test_profile_update_fullname_field(self):
        profile = Profile.objects.get(id=self.profile.id)
        profile.fullname = 'fullnamecreatetest'
        profile.save()
        profile_updated = Profile.objects.get(id=self.profile.id)
        self.assertEquals(profile_updated.fullname, 'fullnamecreatetest')

    def test_profile_update_is_admin_field(self):
        profile = Profile.objects.get(id=self.profile.id)
        profile.is_admin = True
        profile.save()
        profile_updated = Profile.objects.get(id=self.profile.id)
        self.assertTrue(profile_updated.is_admin)

    def test_profile_update_role_field(self):
        profile = Profile.objects.get(id=self.profile.id)
        profile.role = 'EDITOR'
        profile.save()
        profile_updated = Profile.objects.get(id=self.profile.id)
        self.assertEquals(profile_updated.role, 'EDITOR')

    def test_profile_delete_no_cascade(self):
        profile_other = Profile.objects.create(
            user=User.objects.create_user(
                username='user2',
                password='user2',
                email='user2@user2.com',
            ),
            fullname='profile2',
            is_admin=True,
            role='AUTHOR',
        )
        self.assertEquals(Profile.objects.count(), 2)
        self.profile.delete()
        profile_other.delete()
        User.objects.filter(profile=None).delete()
        self.assertEquals(Profile.objects.count(), 0)
        self.assertEquals(User.objects.count(), 0)

    def test_profile_delete_cascade(self):
        profile_other = Profile.objects.create(
            user=User.objects.create_user(
                username='user2',
                password='user2',
                email='user2@user2.com',
            ),
            fullname='profile2',
            is_admin=True,
            role='AUTHOR',
        )
        self.assertEquals(Profile.objects.count(), 2)
        self.profile.user.delete()
        profile_other.user.delete()
        self.assertEquals(Profile.objects.count(), 0)
        self.assertEquals(User.objects.count(), 0)

    def test_profile_unic_user(self):
        self.assertRaises(
            utils.IntegrityError,
            lambda: Profile.objects.create(user=self.user,
                                           fullname='profile2',
                                           is_admin=True,
                                           role='AUTHOR',)
        )


class TestPost(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='Usertest',
            password='admin',
            email='user.test@mail.com',
        )
        self.profile = Profile.objects.create(
            user=self.user,
            fullname='profilefullname',
            is_admin=True,
            role='AUTHOR',
        )
        self.post = Post.objects.create(
            author=self.profile,
            title='Testtitle',
            body='Testbody',
            status='FI',
        )

    def test_post_create_author_field(self):
        post = Post.objects.get(id=self.post.id)
        self.assertEquals(post.author.fullname, 'profilefullname')
        self.assertEquals(post.author.user.username, 'Usertest')

    def test_post_create_title_field(self):
        post = Post.objects.get(id=self.post.id)
        self.assertEquals(post.title, 'Testtitle')

    def test_post_create_body_field(self):
        post = Post.objects.get(id=self.post.id)
        self.assertEquals(post.body, 'Testbody')

    def test_post_create_status_field(self):
        post = Post.objects.get(id=self.post.id)
        self.assertEquals(post.status, 'FI')

    def test_post_create_start_date_field(self):
        post = Post.objects.get(id=self.post.id)
        testdata = date.today()
        self.assertEquals(post.start_date, testdata)

    def test_post_create_update_at_field(self):
        post = Post.objects.get(id=self.post.id)
        self.assertIsNotNone(post.update_at)

    def test_post_create_disabled_field(self):
        post = Post.objects.get(id=self.post.id)
        self.assertFalse(post.disabled)

    def test_post_create_tags_field(self):
        post = Post.objects.get(id=self.post.id)
        self.assertQuerysetEqual(list(post.tags.all()), [])
        self.assertQuerysetEqual(list(Post.tags.all()), [])
        self.assertEqual(post.tags.count(), 0)
        self.assertEqual(Post.tags.count(), 0)
        post.tags.add('testtag')
        self.assertQuerysetEqual(list(post.tags.all()), ['<Tag: testtag>'])
        self.assertEqual(post.tags.count(), 1)
        post.save()
        self.assertQuerysetEqual(list(Post.tags.all()), ['<Tag: testtag>'])
        self.assertEqual(Post.tags.count(), 1)

    def test_post_update_title_field(self):
        post = Post.objects.get(id=self.post.id)
        post.title = 'updatetitletest'
        post.save()
        postupdated = Post.objects.get(id=self.post.id)
        self.assertEquals(postupdated.title, 'updatetitletest')

    def test_post_update_body_field(self):
        post = Post.objects.get(id=self.post.id)
        post.body = 'updatebodytest'
        post.save()
        postupdated = Post.objects.get(id=self.post.id)
        self.assertEquals(postupdated.body, 'updatebodytest')

    def test_post_update_status_field(self):
        post = Post.objects.get(id=self.post.id)
        post.status = 'ED'
        post.save()
        postupdated = Post.objects.get(id=self.post.id)
        self.assertEquals(postupdated.status, 'ED')

    def test_post_update_disabled_field(self):
        post = Post.objects.get(id=self.post.id)
        post.disabled = True
        post.save()
        postupdated = Post.objects.get(id=self.post.id)
        self.assertTrue(postupdated.disabled)

    def test_post_update_tags_field(self):
        post = Post.objects.get(id=self.post.id)
        self.assertQuerysetEqual(list(post.tags.all()), [])
        self.assertQuerysetEqual(list(Post.tags.all()), [])
        self.assertEqual(post.tags.count(), 0)
        self.assertEqual(Post.tags.count(), 0)
        post.tags.add('testtag')
        self.assertQuerysetEqual(post.tags.all(), ['<Tag: testtag>'])
        self.assertEqual(post.tags.count(), 1)
        post.save()
        self.assertQuerysetEqual(Post.tags.all(), ['<Tag: testtag>'])
        self.assertEqual(Post.tags.count(), 1)
        post.tags.add('testtag2')
        self.assertQuerysetEqual(list(post.tags.all()), [
                                 '<Tag: testtag>', '<Tag: testtag2>'])
        self.assertEqual(post.tags.count(), 2)
        post.save()
        self.assertQuerysetEqual(list(Post.tags.all()), [
                                 '<Tag: testtag>', '<Tag: testtag2>'])
        self.assertEqual(Post.tags.count(), 2)

    def test_post_delete_tags_field(self):
        post = Post.objects.get(id=self.post.id)
        self.assertQuerysetEqual(list(post.tags.all()), [])
        self.assertQuerysetEqual(list(Post.tags.all()), [])
        self.assertEqual(post.tags.count(), 0)
        self.assertEqual(Post.tags.count(), 0)
        post.tags.add('testtag')
        self.assertQuerysetEqual(post.tags.all(), ['<Tag: testtag>'])
        self.assertEqual(post.tags.count(), 1)
        post.save()
        self.assertQuerysetEqual(Post.tags.all(), ['<Tag: testtag>'])
        self.assertEqual(Post.tags.count(), 1)
        post.tags.add('testtag2')
        self.assertQuerysetEqual(list(post.tags.all()), [
                                 '<Tag: testtag>', '<Tag: testtag2>'])
        self.assertEqual(post.tags.count(), 2)
        post.save()
        self.assertQuerysetEqual(list(Post.tags.all()), [
                                 '<Tag: testtag>', '<Tag: testtag2>'])
        self.assertEqual(Post.tags.count(), 2)
        post.tags.remove('testtag')
        post.tags.remove('testtag2')
        self.assertQuerysetEqual(list(post.tags.all()), [])
        self.assertEqual(post.tags.count(), 0)
        post.save()
        self.assertQuerysetEqual(list(Post.tags.all()), [])
        self.assertEqual(Post.tags.count(), 0)

    def test_post_delete_cascade(self):
        Post.objects.create(
            author=Profile.objects.create(
                user=User.objects.create_user(
                    username='user',
                    password='user',
                    email='user@mail.com',
                ),
                fullname='profile',
                is_admin=True,
                role='AUTHOR',
            ),
            title='title',
            body='title',
            status='FI',
            tags='testtag',
        )
        self.assertEquals(Post.objects.count(), 2)
        User.objects.all().delete()
        self.assertEquals(Post.objects.count(), 0)

    def test_post_filter_author(self):
        Post.objects.create(
            author=Profile.objects.create(
                user=User.objects.create_user(
                    username='user',
                    password='user',
                    email='user@mail.com',
                ),
                fullname='profile',
                is_admin=True,
                role='AUTHOR',
            ),
            title='title',
            body='title',
            status='FI',
        )
        Post.objects.create(
            author=self.profile,
            title='title',
            body='title',
            status='FI',
            tags='testtag',
        )
        self.assertEquals(Post.objects.count(), 3)
        self.assertEquals(Post.objects.filter(author=self.profile).count(), 2)


class TestTag(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='Usertest',
            password='admin',
            email='user.test@mail.com',
        )
        self.profile = Profile.objects.create(
            user=self.user,
            fullname='profilefullname',
            is_admin=True,
            role='AUTHOR',
        )

    def test_create_tags(self):
        post = Post.objects.create(
            author=self.profile,
            title='Testtitle',
            body='Testbody',
            status='FI',
        )
        post2 = Post.objects.create(
            author=self.profile,
            title='Testtitle',
            body='Testbody',
            status='FI',
        )
        self.assertQuerysetEqual(list(post.tags.all()), [])
        self.assertQuerysetEqual(list(post2.tags.all()), [])
        self.assertQuerysetEqual(list(Post.tags.all()), [])
        self.assertQuerysetEqual(list(Tag.objects.all()), [])
        self.assertEqual(post.tags.count(), 0)
        self.assertEqual(post2.tags.count(), 0)
        self.assertEqual(Post.tags.count(), 0)
        post.tags.add('testtag')
        post.tags.add('testtag2')
        post.save()
        post2.tags.add('testtag2')
        post2.tags.add('testtag3')
        post2.save()
        self.assertQuerysetEqual(list(post.tags.all()), [
                                 '<Tag: testtag>', '<Tag: testtag2>'])
        self.assertQuerysetEqual(list(post2.tags.all()), [
                                 '<Tag: testtag2>', '<Tag: testtag3>'])
        self.assertQuerysetEqual(
            list(Post.tags.all()),
            ['<Tag: testtag>', '<Tag: testtag2>', '<Tag: testtag3>']
        )
        self.assertQuerysetEqual(
            list(Tag.objects.all()),
            ['<Tag: testtag>', '<Tag: testtag2>', '<Tag: testtag3>']
        )
        self.assertEqual(post.tags.count(), 2)
        self.assertEqual(post2.tags.count(), 2)
        self.assertEqual(Post.tags.count(), 3)

    def test_direct_db_tags(self):
        post = Post.objects.create(
            author=self.profile,
            title='Testtitle',
            body='Testbody',
            status='FI',
        )
        post.tags.add('testtag')
        self.assertEqual(post.tags.count(), 1)
        self.assertEqual(Post.tags.count(), 1)
        self.assertQuerysetEqual(list(Tag.objects.all()), ['<Tag: testtag>'])
        tag1 = Tag.objects.all()
        tag1.delete()
        self.assertQuerysetEqual(list(post.tags.all()), [])
        self.assertQuerysetEqual(list(Post.tags.all()), [])
        self.assertQuerysetEqual(list(Tag.objects.all()), [])
        self.assertEqual(post.tags.count(), 0)
        self.assertEqual(Post.tags.count(), 0)
