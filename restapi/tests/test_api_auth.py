from django.test import TestCase
from rest_framework import status, test
from rest_framework.exceptions import AuthenticationFailed, NotAuthenticated

from restapi.models import User


class AuthTest(TestCase):
    def setUp(self):
        self.client = test.APIClient()
        User.objects.create_superuser(
            'user', 'user@user.com', 'user'
        )

    def test_auth_sucess(self):
        self.client.login(username='user', password='user')
        response = self.client.get('/api/auth/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.client.logout()
        response = self.client.get('/api/auth/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        detail = response.data.get('detail')
        self.assertEqual(str(detail), NotAuthenticated.default_detail)

    def test_auth_fail(self):
        response = self.client.get('/api/auth/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        detail = response.data.get('detail')
        self.assertEqual(str(detail), NotAuthenticated.default_detail)

    def test_login_sucess(self):
        response = self.client.post(
            '/api/auth/login/',
            {'username': 'user', 'password': 'user'}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.data.get('user'))
        response = self.client.get('/api/auth/')
        self.assertIsNotNone(response.data.get('user'))

        User.objects.create_user('test', 'test@test.com', 'test')
        response = self.client.post(
            '/api/auth/login/',
            {'username': 'test', 'password': 'test'}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.data.get('user'))
        response = self.client.get('/api/auth/')
        self.assertIsNotNone(response.data.get('user'))

    def test_login_invalid_password(self):
        response = self.client.post(
            '/api/auth/login/',
            {'username': 'user', 'password': 'invalid'}
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        detail = response.data.get('detail')
        self.assertEqual(str(detail), AuthenticationFailed.default_detail)

    def test_login_invalid_params(self):
        response = self.client.post(
            '/api/auth/login/',
            {'username': 'invalid', 'password': 'user'}
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        detail = response.data.get('detail')
        self.assertEqual(str(detail), AuthenticationFailed.default_detail)

        response = self.client.post(
            '/api/auth/login/',
            {'username': 'invalid', 'password': 'user'}
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        detail = response.data.get('detail')
        self.assertEqual(str(detail), AuthenticationFailed.default_detail)

    def test_login_validation(self):
        response = self.client.post(
            '/api/auth/login/',
            {'username': 'user'}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        password = response.data.get('password')
        username = response.data.get('username')
        self.assertIsNotNone(password)
        self.assertIsNone(username)

        response = self.client.post(
            '/api/auth/login/',
            {'password': 'user'}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        password = response.data.get('password')
        username = response.data.get('username')
        self.assertIsNotNone(username)
        self.assertIsNone(password)

        response = self.client.post(
            '/api/auth/login/',
            {}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        password = response.data.get('password')
        username = response.data.get('username')
        self.assertIsNotNone(username)
        self.assertIsNotNone(password)
