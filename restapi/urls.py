from django.urls import path, re_path
from rest_framework import routers

from restapi.views import (
    AuthLoginView, AuthLogoutView, AuthView,
    NotFoundView, UserViewSet, PostViewSet,
    TagsListView, TagView, PosttagsListView,
)


router = routers.DefaultRouter()
router.register(r'users', UserViewSet, 'user')
router.register(r'posts', PostViewSet, 'post')


urlpatterns = router.urls
urlpatterns += [
    path('auth/', AuthView.as_view(), name='auth'),
    path('auth/login/', AuthLoginView.as_view(), name='auth-login'),
    path('auth/logout/', AuthLogoutView.as_view(), name='auth-logout'),
    path('posts/tags/<pk>/', PosttagsListView.as_view(), name='tag-list-posts'),
    path('tags/', TagsListView.as_view(), name='tag-list'),
    path('tags/<pk>/', TagView.as_view(), name='tag-detail'),
    re_path('^.*/$', NotFoundView.as_view(), name='not-found'),
]
