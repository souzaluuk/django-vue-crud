# from django.contrib import admin
from django.urls import path, re_path, include
from django.shortcuts import render
from django.views.decorators.csrf import ensure_csrf_cookie


def index(request): return render(request, 'index.html')


urlpatterns = list()
# urlpatterns.append(path('admin/', admin.site.urls))
urlpatterns.append(path('api/', include('restapi.urls')))
urlpatterns.append(re_path(r'^(?!api/|admin/).*$', ensure_csrf_cookie(index)))
