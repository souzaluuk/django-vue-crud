# Crud usando Django + VueJS

* Python 3.8
* Vue-CLI 4.3.1

## Instalação do servidor Django

Clone este repositório e vá para a raiz do projeto:

```bash
git clone https://gitlab.com/souzaluuk/django-vue-crud
cd django-vue-crud
```

Para iniciar, crie um ambiente virtual do Python e instale as dependências:

```bash
python -m venv env
source env/bin/activate
pip install -r requirements.txt
```

## Construa os arquivos estáticos do VueJS

Siga as instruções em [./client/REDME.md](client/README.md), e construa os arquivos estáticos com:

```bash
cd client
npm install
npm run build
```

Após as instruções, os arquivos estáticos serão criados em `django-vue-crud/client/dist`.

## Configurações locais

Crie o arquivo de configuração `.env` na raíz do projeto (`django-vue-crud`), use como parâmetro o arquivo [`.env.example`](.env.example)

## Execução da aplicação

A partir da raíz do projeto, execute:

```bash
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```