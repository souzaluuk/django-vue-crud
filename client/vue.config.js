module.exports = {
  devServer: {
    port: 3000,
    proxy: {
      '^/api/': {
        target: 'http://127.0.0.1:8000/',
        ws: false
      }
    }
  },
  // outputDir deve ser adicionado a lista TEMPLATE_DIRS do settings.py do Django's
  outputDir: process.env.NODE_ENV === 'production' ? '../public' : 'dist',
  // assetsDir deve corresponder ao STATIC_URL no settings.py do Django's
  assetsDir: 'static'
}
