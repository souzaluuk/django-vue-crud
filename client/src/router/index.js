import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/post/:id+',
    name: 'ShowPost',
    component: () =>
      import(/* webpackChunkName: "showPost" */ '../views/ShowPost.vue')
  },
  {
    path: '/create',
    name: 'CreatePost',
    component: () =>
      import(/* webpackChunkName: "createPost" */ '../views/NewPost.vue')
  },
  {
    path: '/edit/:id+',
    name: 'EditPost',
    component: () =>
      import(/* webpackChunkName: "editPost" */ '../views/EditPost.vue')
  },
  {
    path: '/posts',
    name: 'AllPosts',
    component: () =>
      import(/* webpackChunkName: "allPosts" */ '../views/AllPosts.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () =>
      import(/* webpackChunkName: "login" */ '../views/Login.vue')
  },
  {
    path: '/signin',
    name: 'SignIn',
    component: () =>
      import(/* webpackChunkName: "signin" */ '../views/SignIn.vue')
  },
  {
    path: '/users',
    name: 'AllUsers',
    component: () =>
      import(/* webpackChunkName: "users" */ '../views/AllUsers.vue')
  },
  {
    path: '/user/:id+',
    name: 'User',
    component: () =>
      import(/* webpackChunkName: "user" */ '../views/ShowUser.vue')
  },
  {
    path: '/edit-user/:id+',
    name: 'EditUser',
    component: () =>
      import(/* webpackChunkName: "editUser" */ '../views/EditUser.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
